#ifndef __FACTORIAL_H__
#define __FACTORIAL_H__

/* compile with (from deps/lib):
 * gcc -shared -o libfactorial.so ../src/factorial.o
 */

extern int factorial(int x);

#endif // __FACTORIAL_H__

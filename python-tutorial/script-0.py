#!/usr/bin/env python

print('Hello world!')

a = 1
b = 2
c = a + b
print(c)

H = "Hopkins"
print(H)
print(len(H))
print(H[0])
print(H[1])
print(H[-1])
print(H[0:3])

print(H + ' University')
print(H * 4)
#print(H - 4)
print(H.find('pki'))

T = True
F = False
N = None

print(T == F)

# Lists

L = ['JHU', 1876, None]
print(L)
L.append('Baltimore')
print(L)
L.pop(2)
print(L)
L[1] = 'Homewood'
print(L)
L.sort()
print(L)

# Dictionaries
D = {
  'school': 'Hopkins',
  'nstudents': 12345,
}
print(D['school'])
print(D)
D['city'] = 'Baltimore'
print(D)

patients = list()
for i in range(5):
  G = {
    'patient': i,
    'height': 5.9*i,
    'weight': 123-i,
  }
  patients.append(G)

print(patients[0]['height'])

print(list(range(5)))

for patient in patients[::2]:
  for key in patient:
    print(key, patient[key])

print("that's all")

f = open('readme', 'r')
text = f.read()
print(text)

f.close()
f = open('readme', 'r')

for line in f:
  print(line)
  print(line.split('g'))



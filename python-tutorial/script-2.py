#!/usr/bin/env python

# without alias, must use entire import path in function call (e.g., lib.get_input())
# this import reads ALL functions in my_functions.py
#import my_library.my_functions as lib

# a more selective way of importing
from my_library.my_functions import get_input, check_length

user_in = get_input()

# repeat until a "q" appears
# find returns -1 if not found in string
while user_in.find('q') == -1:
  print(user_in)
  check_length(user_in)
  user_in = get_input()

print('Quit')

#!/usr/bin/env python

#############
# functions #
#############

# define an input query
def get_input():
  return input("Type some words: ")

# print the length of the string
def check_length(string):
  print(len(string))

################
# begin script #
################

user_in = get_input()

# repeat until a "q" appears
# find returns -1 if not found in string
while user_in.find('q') == -1:
  print(user_in)
  check_length(user_in)
  user_in = get_input()

print('Quit')

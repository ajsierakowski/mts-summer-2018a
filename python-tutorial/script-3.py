#!/usr/bin/env python

# classes

# class definition
class Animal():
  name = None
  sound = None
  num_legs = 4

  def __str__(self):
    return str(self.name)

  def make_sound(self):
    print(self.sound)

  def teach_sound(self, new_sound):
    self.sound = new_sound

class Cat(Animal):
  name = 'cat'
  sound = 'meow'
  eye_color = 'yellow'

# instantiate class
anim1 = Animal()
print(anim1)
anim1.name = 'prototypical animal'
print(anim1)
anim1.make_sound()
anim1.teach_sound('Ah!')
anim1.make_sound()

cat1 = Cat()
print(cat1)
cat1.make_sound()
